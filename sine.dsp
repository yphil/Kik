declare name "osc";
declare nvoices "16";

import("basics.lib");
import("oscillators.lib");
import("envelopes.lib");

gate	= button("/gate");							// 0/1
// Voice controls
freq	= vslider("/h:Kicker/Freq", 56, 20, 2000, 1);	// Hz
gain	= vslider("/h:Kicker/gain", 0.3, 0, 10, 0.01);				// %

// Relative amplitudes of the different partials
amp(1)	= vslider("/h:Kicker/amp1", 3, 0, 3, 0.01);
amp(2)	= vslider("/h:Kicker/amp2", 0, 0, 3, 0.01);
amp(3)	= vslider("/h:Kicker/amp3", 0, 0, 3, 0.01);

// Envelope controls
attack	= vslider("/h:Kicker/Attack", 0.001, 0, 1, 0.001);		// sec
release = vslider("/h:Kicker/Release", 0.30, 0, 1, 0.001);		// sec

// Master controls (volume and stereo panning)
vol	= 0.8;

// partial #i (0..n-1)
partial(i) = amp(i+1)*osc((i+1)*freq);

smooth(s) = *(1.0 - s) : + ~ *(s);
s = 0.993;
r = vslider("Range[enum:Low|High]", 0, 0, 1,1) ;
range = vslider("Depth[name:Depth]", 1.0, 0, 1, 0.01) : smooth(s);
Inverted(b, x) = if(b, 1 - x, x);
peak = hslider("Peak[name:Peak]", 5, -6, 20, 0.1) :> component("music.lib").db2linear : smooth(s);

process = hgroup("Kicker"
                     ,sum(i, 3, partial(i))
                     * vgroup("Control",gain * (gate : ar(attack, release)))
                );
