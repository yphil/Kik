import("oscillators.lib");
import("envelopes.lib");
import("maths.lib");
import("noises.lib");

freq = vslider("/h:Noise/Freq", 440, 20, 20000, 1);	            // Hz
bw = vslider("/h:Noise/Bandwidth (Hz)", 100, 20, 20000, 10);

attack		= vslider("/h:Noise/A", 0.01,0, 1, 0.001);	        // sec
decay		= vslider("/h:Noise/D",0.3, 0, 1, 0.001);	        // sec
sustain	= vslider("/h:Noise/S",0.5, 0, 1, 0.01);	            // frac
release	= vslider("/h:Noise/R",0.2, 0, 1, 0.001);           	// sec

gain = 1;							                            // frac
gate = button("gate");					                        // 0/1

noizer = noise * env * gain : filter
with {
    env = gate :
        hgroup("Noise", adsr(attack, decay, sustain, release));
    filter = vgroup("Noise Filter", (firpart : + ~ feedback));
    R = exp(-PI*bw/SR);                                          // pole radius
    A = 2*PI*freq/SR;                                            // pole angle (radians)
    RR = R*R;
    firpart(x) = (x - x'') * (1-RR)/2;
    // time-domain coefficients ASSUMING ONE-SAMPLE FEEDBACK DELAY:
    feedback(v) = 0 + 2*R*cos(A)*v - RR*v';
};

process = noizer;
